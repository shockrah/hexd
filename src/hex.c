#include "hex.h"

static uint8_t file_buffer[FILE_BUFFER_SIZE];

void basic_hex(FILE* elf, const size_t offset){
	assert(elf != NULL);
	
	// by default begin seeking from the given offset
	fseek(elf, offset, SEEK_SET);
	
	while(!feof(elf)) {
		if(fread(file_buffer, 0x04, FILE_BUFFER_SIZE, elf)) {
			//dump out oneline
			// check for optional values
			for(size_t i =0; i<FILE_BUFFER_SIZE; i++) {
				if(i%LINE_LENGTH)
					putchar('\n');
				else
					printf("%02x ",file_buffer[i]);
			}
		}
	}
}

#include <assert.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
// This whole file will assume that we are looking for a valid ELF
// from a base offset of 0x00
#define MaxHeaderLength 64
#define MagicLength 4

// offsets for more readability in implementation
#define ARCH_OFFSET 4
#define ENCODING_OFFSET 5
#define OSABI_OFFSET 6

#define ELF_TYPE_OFFSET 16
#define ISA_OFFSET 18
#define ELF_VERSION_OFFSET 20 #define PROGRAM_ENTRY_OFFSET 24
#define PROG_TABLE_POSITION_OFFSET 32
#define SECTION_HEADER_TABLE_OFFSET 40

#define FLAGS_OFFSET 48
#define HEADER_SIZE_OFFSET 52

#define PROG_ENTRY_SIZE_OFFSET 54
#define PROG_ENTRY_COUNT_OFFSET 56

#define SECT_ENTRY_SIZE_OFFSET 58
#define SECT_ENTRY_COUNT_OFFSET 60

#define SECT_HEAD_INDEX_OFFSET 62

// container for all 64 bit header data
typedef struct{
	uint8_t magic_0;
	uint8_t magic_1;
	uint8_t magic_2;
	uint8_t magic_3;

	uint8_t architecture;
	uint8_t encoding;
	uint8_t osABI;

	uint64_t padding;	// unused padding

	uint16_t elfType;
	uint16_t instructionSet;
	uint32_t elfVersion;

	uint64_t progEntry;
	uint64_t progTablePosition;
	uint64_t sectionHeaderTable;

	uint32_t flags;
	uint16_t headerSize;

	uint16_t progHeaderEntrySize;
	uint16_t progHeaderEntyCount;

	uint16_t sectHeaderEntrySize;
	uint16_t sectHeaderEntryCout;

	uint16_t sectNamesIdx;
}HeaderData64_t;

/* Program header for both so and exe's */
typedef struct {
	uint32_t type; // if this is null we should ignore the rest of the values
	uint32_t flags;
	// offset from start of _file_ to start of this segment's btye
	uint64_t offset;
	// virtual address of the segment in memory
	uint64_t vaddr;
	// some systems need physical addressing so we leave this here
	// as a form of a physical address(embedded)
	uint64_t paddr;
	// both of these are allowed to be zero
	uint64_t fileSize;	// size in bytes of file
	uint64_t memsize;	// size in bytes of memory image of segment

	uint64_t align;		// alignment off (vaddr) % (page size)
}ProgramHeader64_t;

typedef struct {
	uint32_t sh_name;
	uint32_t sh_type;
	uint64_t sh_flags;

	uint64_t sh_addr;
	uint64_t sh_offset;

	uint64_t sh_size;
	uint64_t sh_link;
	uint64_t sh_info;

	uint64_t sh_addralign;
	uint64_t sh_entsize;
}SectionHeader64_t;


/* Pointer to populated header struct */
HeaderData64_t* pullHeaderData(FILE* fileName);

SectionHeader64_t* pullSectionHeader(HeaderData64_t*, FILE*);

void showHeader(const HeaderData64_t* head);

void headerInfoDispatch(FILE* file);

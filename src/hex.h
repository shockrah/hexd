/* 
 * All the hex dumping utilies are found in this module
 * Implementation in hex.c
 * */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <stdint.h>

#ifndef ADEBUG
	#define NDEBUG
#endif

// reading 1 page at a time
#define FILE_BUFFER_SIZE 4096
#define LINE_LENGTH 16

void basic_hex(FILE* elf, const size_t offset);
